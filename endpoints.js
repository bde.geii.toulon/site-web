module.exports = [
  {
    path: "/formulaire",
    page: "formulaire",
    handler: "inscription",
    method: "POST",
    contentType: "application/json"
  }
]