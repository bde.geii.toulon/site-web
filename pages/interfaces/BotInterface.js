module.exports = class BotInterface {
  constructor(n) {
    this.n = n;

    this.config = n.config;
    this.log = n.log;
  }

  init(message, ws) {
    if(message.code == this.n.discordCreds.wsCode) {
      this.ws = ws;
      this.log.info("connected to bot");
      this.send({ type: "authok" });
    } else return;
  }

  send(message) {
    if(!this.ws) return;
    if(!message.type) throw "No type on message";
    this.ws.send(JSON.stringify(message));
  }
}