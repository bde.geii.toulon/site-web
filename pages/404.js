const Page = require("../engine/Page");

module.exports = class NotFoundPage extends Page {
  constructor(n, renderer) {
    super(n, renderer);
  }

  render(req, res, html) {
    const top = this.r.top({
      title: `Page non trouvée - ${this.siteName}`,
      description: `${this.siteDesc}`
    });

    const menu = this.r.menu();

    const content = html;

    const bottom = this.r.bottom();

    res.writeHead(404);

    res.end([
      top,
      menu,
      content,
      bottom
    ].join(""));
  }
}
