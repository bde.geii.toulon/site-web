const Page = require("../engine/Page");

module.exports = class ContactPage extends Page {
  constructor(n, renderer) {
    super(n, renderer);
  }

  async render(req, res, html, query) {
    const top = this.r.top({
      title: `Contact - ${this.siteName}`,
      description: `${this.siteDesc}`
    });

    const menu = this.r.menu({
      selectedItem: "Contact"
    });

    const content = html;

    const bottom = this.r.bottom();

    res.end([
      top,
      menu,
      content,
      bottom
    ].join(""));
  }
}
