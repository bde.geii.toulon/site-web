const Page = require("../engine/Page");

module.exports = class JoinPage extends Page {
  constructor(n, renderer) {
    super(n, renderer);
  }

  async render(req, res, html, query) {
    const top = this.r.top({
      title: `Rejoindre le staff du BDE - ${this.siteName}`,
      description: `${this.siteDesc}`
    });

    const menu = this.r.menu();

    const content = html;

    const bottom = this.r.bottom();

    res.end([
      top,
      menu,
      content,
      bottom
    ].join(""));
  }
}
