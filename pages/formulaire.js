const Page = require("../engine/Page");

const url = require("url");
const https = require("https");
const { db } = require("../config");

module.exports = class FormulairePage extends Page {
  constructor(n, renderer) {
    super(n, renderer);

    this.discordRedirectURI = this.config.dev ? "http://127.0.0.1:8095/formulaire" : "https://bdegeii.y3n.co/formulaire";
    this.discordRedirectURIEncoded = encodeURIComponent(this.discordRedirectURI);
    this.discordOAuthURI = `https://discord.com/oauth2/authorize?client_id=754043053974618262&redirect_uri=${this.discordRedirectURIEncoded}&response_type=code&scope=identify%20email%20guilds.join`;
  }

  async render(req, res, html, query) {
    const urlObj = new URLSearchParams(query);
    const code = urlObj.get("code");
    const auth = await this.checkCode(code);

    if (auth == null) {
      res.setHeader('Location', this.discordOAuthURI);
      res.writeHead(302);
      res.end();
      return;
    }

    const member = await this.createMember(auth);
    if(member.awaitingVerification || member.active) {
      res.setHeader('Location', '/rejoindre?already');
      res.writeHead(302);
      res.end();
      return;
    }

    html = this.rep(html, "id", member.id);
    html = this.rep(html, "jeton", member.jeton);

    const top = this.r.top({
      title: `Formulaire membre - ${this.siteName}`,
      description: `${this.siteDesc}`
    });

    const menu = this.r.menu();

    const content = html;

    const bottom = this.r.bottom();

    res.end([
      top,
      menu,
      content,
      bottom
    ].join(""));
  }

  async inscription(req, res, body) {
    try {
      const formattedSet = {
        etudEmail: body.etudEmail,
        prenom: body.prenom,
        nom: body.nom,
        annee: parseInt(body.annee),
        groupe: body.groupe,
        email: body.email,
        genre: parseInt(body.genre),
        dateNaissance: new Date(body.dateNaissance),
        noteFormulaire: parseInt(body.note),
        awaitingVerification: true,
        active: false,
        finished: true
      }

      if(typeof body.id !== "string" || typeof body.jeton !== "string") throw "No id/jeton";
      await this.db.collection("members").updateOne({ id: body.id, jeton: body.jeton }, { $set: formattedSet });
    } catch (e) {
      console.error(e);
      res.writeHead(400);
      res.end();
      return;
    }

    this.n.bot.send({ type: "verification", data: await this.db.collection("members").findOne({ id: body.id }) });

    res.setHeader("content-type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({ success: true }));
  }

  async createMember(auth) {
    const options = {
      method: "GET",
      headers: {
        'authorization': `${auth.token_type} ${auth.access_token}`
      }
    }

    let [statusCode, body] = await this.req("https://discordapp.com/api/users/@me", options);
    if (statusCode == 200) {
      body = JSON.parse(body);

      await this.db.collection("members").updateOne({ discordID: body.id }, {
        $set: {
          discordID: body.id,
          discordUsername: body.username,
          discordDiscriminator: body.discriminator,
          discordAvatar: body.avatar,
          discordEmail: body.email,
          discordVerifiedEmail: body.verified,
          editDate: new Date(),
          finished: false
        },
        $setOnInsert: {
          id: this.idGen.create(),
          jeton: this.idGen.create(),
          creationDate: new Date()
        }
      }, { upsert: true });

      const member = await this.db.collection("members").findOne({ discordID: body.id });

      await this.db.collection("discordTokens").updateOne({ memberID: member.id }, {
        $set: {
          memberDiscordID: member.discordID,
          access_token: auth.access_token,
          token_type: auth.token_type,
          refresh_token: auth.refresh_token,
          editDate: new Date()
        },
        $setOnInsert: {
          creationDate: new Date()
        }
      }, { upsert: true });

      return member;
    } else {
      return null
    }
  }

  async checkCode(code) {
    if (code === null || code === "") {
      return null;
    }

    const data = new URLSearchParams({
      client_id: this.n.discordCreds.clientID,
      client_secret: this.n.discordCreds.secret,
      grant_type: 'authorization_code',
      redirect_uri: this.discordRedirectURI,
      code: code,
      scope: 'identify email guilds.join',
    }).toString();

    const options = {
      method: "POST",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    }

    let [statusCode, body] = await this.req("https://discord.com/api/oauth2/token", options, data);
    if (statusCode == 200) {
      return JSON.parse(body);
    } else {
      return null;
    }
  }

  req(url, options, data) {
    return new Promise((r) => {
      const req = https.request(url, options, (res) => {
        res.setEncoding('utf8');

        let body = "";
        res.on('data', (chunk) => {
          body += chunk;
        });
        res.on('end', () => {
          r([res.statusCode, body])
        });
      });

      req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
      });

      if (data) req.write(data);
      req.end();
    });
  }
}
