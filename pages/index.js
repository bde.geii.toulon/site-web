const Page = require("../engine/Page");
const BotInterface = require("./interfaces/BotInterface");
const discordCreds = require("../discordCreds");

module.exports = class IndexPage extends Page {
  constructor(n, renderer) {
    super(n, renderer);

    this.n.discordCreds = discordCreds;
    this.n.bot = new BotInterface(this.n);
    this.bot = this.n.bot;
  }

  render(req, res, html) {
    const top = this.r.top({
      title: `Accueil - ${this.siteName}`,
      description: `${this.siteDesc}`
    });

    const menu = this.r.menu({
      selectedItem: "Home"
    });

    const content = html;

    const bottom = this.r.bottom();

    res.end([
      top,
      menu,
      content,
      bottom
    ].join(""));
  }

  botauth(message, ws) {
    this.n.bot.init(message, ws);
  }
}
