const Logger = require("./Logger");
const HTTPServer = require("./HTTPServer");
const ConfigHandler = require("./handlers/ConfigHandler")
const Database = require("./Database");
const ID = require("./ID");
//const Serializer = require("./Serializer");
const Renderer = require("./Renderer");

const pages = require("../pages");
const endpoints = require("../endpoints");
const messages = require("../messages");

module.exports = class NoctaneSR {
  constructor(config) {
    this.version = "1.0.0";

    this.config = config;
    this.c = config;

    this.pages = pages;
    this.endpoints = endpoints;
    this.messages = messages;
  }

  async start() {
    this.startDate = new Date();
    this.log = new Logger(this.c);
    this.idGen = new ID();

    this.log.info(`starting ${this.c.name} server ${this.version}`);

    this.initConfig();

    /*this.endpoints = endpoints;

    this.s = new Serializer(this);
    await this.s.init();*/

    await this.initDB();

    this.r = new Renderer(this);
    await this.r.prepare();

    await this.initServer();

    this.log.info(`server started in ${new Date() - this.startDate} ms`);
  }

  initConfig() {
    this.configHandler = new ConfigHandler(this);
    this.configHandler.load();
  }

  async initDB() {
    this.dbManager = new Database(this);
    this.db = await this.dbManager.dbConnect();
  }

  async initServer() {
    this.httpServer = new HTTPServer(this);
    this.httpServer.start();
  }
}
