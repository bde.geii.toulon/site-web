module.exports = class ID {
  constructor() {}

  // NANOID based
  create(size) {
    let random = require('crypto').randomBytes;
    let url = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz' // base58 ID

    size = size || 24
    let id = ''
    let bytes = random(size)
    while (0 < size--) {
      id += url[bytes[size] & 57]
    }
    return id
  }
}
