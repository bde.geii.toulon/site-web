module.exports = class Page {
  constructor(n, renderer) {
    this.n = n;
    this.r = renderer;
    this.log = this.n.log;
    this.siteName = this.n.config.siteName;
    this.siteDesc = this.n.config.siteDesc;
    this.config = n.config;
    this.db = n.db;
    this.idGen = n.idGen;
  }

  rep(str, id, value) {
    return str.replace(`{{${id}}}`, value);
  }
}