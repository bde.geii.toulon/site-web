const MenuRenderer = require("../modules/menu");
const fs = require("fs").promises;

module.exports = class Renderer {
  constructor(n) {
    this.n = n;
    this.pages = n.pages;

    this.log = this.n.log;
  }

  async prepare() {
    Object.keys(this.pages).forEach(async (pageURL) => {
      const page = this.pages[pageURL];
      if(page.active == false) return;

      this.log.verbose(`preparing page ${pageURL}`);
      
      const RendererI = require(`../pages/${page.renderer}`);
      page.html = await this.readHTMLPage(`./html/${page.renderer}.html`);
      page.renderer = new RendererI(this.n, this);
      if(typeof page.renderer.prepare == "function") {
        page.renderer.prepare();
      }
    })

    this.topHTML = await fs.readFile("./html/top.html", "utf-8");
    this.menuRenderer = new MenuRenderer(this.n, this, await fs.readFile("./html/menu.html", "utf-8"));
    this.bottomHTML = await fs.readFile("./html/bottom.html", "utf-8");
  }

  async readHTMLPage(path) {
    try {
      return await fs.readFile(path, "utf-8");
    } catch(e) {
      return null;
    }
  }

  menu(options) {
    return this.menuRenderer.render(options);
  }

  top(options) {
    let top = this.topHTML.slice(0);
    top = this.rep(top, "lang", options.lang || this.n.config.defaultLanguage);
    top = this.rep(top, "title", options.title || "Default title");
    top = this.rep(top, "description", options.description || "Default description");
    return top;
  }

  bottom(options) {
    let bottom = this.bottomHTML.slice(0);

    return bottom;
  }

  rep(str, id, value) {
    return str.replace(`{{${id}}}`, value);
  }
}
