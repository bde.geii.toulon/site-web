module.exports = class PageHandler {
  constructor(n) {
    this.n = n;

    this.pages = this.n.pages;

    this.config = n.config;
    this.log = n.log;
  }

  handle(req, res) {
    let [path, query] = req.url.split('?');
    let page = this.pages[path];

    if(!page && req.url.startsWith("/player/")) {
      page = this.pages["/player"];
    }

    if(!page) {
      res.setHeader('Location', '/404');
      res.writeHead(302);
      res.end();
      return;
    }

    if(page.active == false) {
      res.setHeader('Location', '/400');
      res.writeHead(302);
      return;
    }

    if(!page.renderer || !page.renderer.render) {
      res.setHeader('Location', '/500');
      res.writeHead(302);
      return;
    }

    this.log.verbose(`pageHandler handling ${req.url}`);

    res.setHeader('Content-Type', 'text/html');

    const html = page.html ? page.html.slice(0) : null; // copy of html
    page.renderer.render(req, res, html, query);
  }
}
