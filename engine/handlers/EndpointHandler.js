module.exports = class EndpointHandler {
  constructor(n) {
    this.n = n;

    this.pages = this.n.pages;
    this.endpoints = this.n.endpoints;

    this.config = n.config;
    this.log = n.log;
  }

  handle(req, res, body) {
    this.log.verbose(`handling endpoint ${req.url}`);
    for(const endpoint of this.endpoints) {
      if(req.url == endpoint.path && req.method == endpoint.method) {
        const page = this.pages[`/${endpoint.page}`];
        if(typeof page == "object") {
          const handler = page.renderer[endpoint.handler];
          if(typeof handler == "function") {
            page.renderer[endpoint.handler](req, res, body);
            return;
          }
        }
      }
    }

    res.setHeader('Location', '/404');
    res.writeHead(302);
    res.end();
  }
}