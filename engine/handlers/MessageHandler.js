module.exports = class MessageHandler {
  constructor(n) {
    this.n = n;

    this.config = n.config;
    this.log = n.log;
    this.s = n.s;

    this.pages = this.n.pages;
    this.messages = this.n.messages;
  }

  handle(message, ws) {
    message = this.decode(message);
    if(!message || !message.type) return;

    for(const messageDef of this.messages) {
      if(messageDef.type == message.type) {
        const page = this.pages[`/${messageDef.page == "index" ? "" : messageDef.page}`];
        if(typeof page == "object") {
          const handler = page.renderer[messageDef.handler];
          if(typeof handler == "function") {
            this.log.verbose(`messageHandler handling ${message.type}`);
            page.renderer[messageDef.handler](message, ws);
            return;
          }
        }
      }
    }
  }

  decode(message) {
    try {
      return JSON.parse(message);
    } catch(e) {
      console.error(e);
    }
  }
}