const PageHandler = require("./PageHandler");
const EndpointHandler = require("./EndpointHandler");
const fs = require('fs');

module.exports = class RequestHandler {
  constructor(n) {
    this.n = n;

    this.pageHandler = new PageHandler(this.n);
    this.endpointHandler = new EndpointHandler(this.n);

    this.config = n.config;
    this.log = n.log;
    this.s = n.s;
  }

  handle(req, res, buffer) {
    if(req.method == "GET") {
      if(req.url == "/favicon.ico") {
        this.handleFavicon(req, res);
        return;
      }

      if(/^\/s\/.*\.(png|css|svg)$/.test(req.url)) {
        this.handleFile(req, res);
        return;
      }
      
      this.handlePage(req, res);
      return;
    }

    if(req.headers["content-type"] == "application/x-protobuf") {
      this.handlePB(req, res, buffer);
      return;
    }

    if(req.headers["content-type"] == "application/json") {
      this.handleJSON(req, res, buffer);
      return;
    }

    res.setHeader('Location', '/400');
    res.writeHead(302);
    res.end();

    return;
  }

  async handleFile(req, res) {
    const path = `./assets/${req.url.slice(3)}`;

    //console.log(path);

    fs.access(path, (err) => {
      if(err) {
        this.log.verbose(`file 404`);
        res.setHeader('Location', '/404');
        res.writeHead(302);
        res.end();
        return;
      }

      const fileStream = fs.createReadStream(path);

      const fileExt = path.split('.').pop();
      //console.log(fileExt);

      if(fileExt == "png") res.setHeader('content-type', 'image/png');
      else if(fileExt == "css") res.setHeader('content-type', 'text/css');
      else if(fileExt == "svg") res.setHeader('content-type', 'image/svg+xml');
      else res.setHeader('content-type', 'application/octet-stream');
      res.setHeader('cache-control', 'max-age=86400');

      fileStream.pipe(res);
    });
  }

  handlePage(req, res) {
    this.pageHandler.handle(req, res);
    return;
  }

  handleFavicon(req, res) {
    const fileStream = fs.createReadStream("./assets/favicon.ico");
    res.setHeader("Content-Type", "image/x-icon");
    fileStream.pipe(res);
  }

  handleJSON(req, res, buffer) {
    try {
      const parsed = JSON.parse(buffer);
      this.endpointHandler.handle(req, res, parsed);
    } catch(e) {
      console.log(e);
      res.writeHead(400);
      res.end();
      return;
    }
  }

  handlePB(req, res, buffer) {
    this.decodePB(buffer);

    res.writeHead(200);
    res.end();
  }

  decodePB(buffer) {
    try {
      const decodedEnvelope = this.s.decode("Request", buffer);
      const req = this.s.decode(decodedEnvelope.requestType, decodedEnvelope.messageData);
      return {
        envelope: decodedEnvelope,
        req: req
      }
    } catch(e) {
      this.log.verbose(e);
      return;
    }
  }
}
