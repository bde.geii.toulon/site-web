const http = require("http");
const WebSocket = require('ws');

const RequestHandler = require("./handlers/RequestHandler");
const MessageHandler = require("./handlers/MessageHandler");

module.exports = class HTTPServer {
  constructor(n) {
    this.n = n;

    this.config = n.config;
    this.log = n.log;

    this.requestHandler = new RequestHandler(n);
    this.messageHandler = new MessageHandler(n);
  }

  start() {
    this.log.info(`Starting http server`);

    this.server = http.createServer((req, res) => {
      let buffers = [];
      req.on('data', (data) => {
        buffers.push(data);
      });

      req.on('end', () => {
        let buffer = Buffer.concat(buffers);
        this.requestHandler.handle(req, res, buffer);
      });
    });

    this.server.setTimeout(4000);
    if (this.config.ws.enabled) this.setupWS();

    this.server.on('clientError', (err, socket) => {
      socket.end();
    });
    //this.server.maxHeadersCount = 1;
    this.server.listen(this.config.port, this.config.hostname || "127.0.0.1");

    this.log.info(`HTTP server listening on port ${this.config.port}`);
  }

  setupWS() {
    this.wss = new WebSocket.Server({ server: this.server });

    this.wss.on('connection', (ws) => {
      ws.on('message', (message) => {
        this.messageHandler.handle(message, ws);
      });

      ws.on('pong', () => {
        ws.isAlive = true;
      });
    });

    const interval = setInterval(() => {
      this.wss.clients.forEach((ws) => {
        if (ws.isAlive === false) return ws.terminate();

        ws.isAlive = false;
        ws.ping();
      });
    }, 5000);
  }
}
