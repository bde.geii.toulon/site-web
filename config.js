let config = {
  dev: process.env.NODE_ENV == "production" ? false : true,
  port: 8095,
  hostname: process.env.NODE_ENV == "production" ? "127.0.0.1" : "0.0.0.0",
  caching: {
    memoryCache: true
  },
  log: {
    info: true,
    warn: true,
    verbose: true,
    prefix: "siteBDE"
  },
  name: "siteBDE",
  siteName: "BDE GEII TOULON",
  siteDesc: "Bureau des étudiants de GEII à l'IUT de Toulon",
  db: {
    name: "bde"
  },
  defaultLanguage: "fr",
  ws: {
    enabled: true
  }
}

module.exports = config;
