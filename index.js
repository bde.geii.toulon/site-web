const NoctaneSR = require("./engine/NoctaneSR");
const config = require("./config");

(async () => {
  const nsr = new NoctaneSR(config);
  await nsr.start();
})();

