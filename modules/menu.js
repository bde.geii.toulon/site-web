module.exports = class MenuRenderer {
  constructor(n, renderer, html) {
    this.n = n;
    this.renderer = renderer;
    this.html = html;
  }

  render(options) {
    let menu = this.html.slice(0);

    if(options && options.selectedItem) {
      menu = menu.replace(`{{is${options.selectedItem}}}`, " selectedMenuItem");
    }

    menu = menu.replace(/{{[A-z0-1]+}}/g, "");

    return menu;
  }
}
