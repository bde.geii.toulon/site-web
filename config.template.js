let config = {
  log: 1,
  debug: 1,
  devExplorer: 1,
  dataPath: "data/",
  compression: true,
  hostname: "0.0.0.0",
  port: 8095,
  steamAPIKey: "x",
  caching: {
    memoryCache: true
  },
  log: {
    info: true,
    warn: true,
    verbose: true
  }
}

module.exports = config;
