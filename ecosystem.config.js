module.exports = {
  apps: [
    {
      name: "bde.site-web",
      script: "./index.js",
      watch: true,
      env: {
        "NODE_ENV": "production"
      }
    }
  ]
}