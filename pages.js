module.exports = {
  "/": {
    endpoints: [],
    renderer: "index",
  },
  "/404": {
    renderer: "404",
  },
  "/rejoindre": {
    endpoints: [],
    renderer: "rejoindre"
  },
  "/formulaire": {
    endpoints: [],
    renderer: "formulaire"
  },
  "/recrutement": {
    endpoints: [],
    renderer: "recrutement"
  },
  "/contact": {
    endpoints: [],
    renderer: "contact"
  }
}
